//
//  ViewController.swift
//  ABC123
//
//  Created by Daniel Ungerfält on 12/04/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//

import UIKit
import GameplayKit
import AVFoundation

class ViewController: UIViewController  {
    
    var bgImage: UIImageView?
    var letterIndex = [Int](1...8)
    var image: UIImageView?
    var letterNumber = 1
    var correctAnswers = 0
    var upperX: CGFloat = 85
    var upperY: CGFloat = 120
    var positionX : [CGFloat] = [208,114,20,302,302,20,208,114]
    var positionY : [CGFloat] = [450,550,450,550,450,550,550,450]
    var letterImage: [UIButton] = []
    var tabelle : [UIButton] = []
    var newLetterTimer = NSTimer()
    var removeLetterTimer = NSTimer()
    var soundTimer = NSTimer()
    var SuccesTimer = NSTimer()
    var math = MathClass()
    var abc = Abc()
    var nextLetterSpan = false
    var letterSound: AVAudioPlayer!
    var soundUrl: NSURL!
    var currentLetter: Int = 0
    
    @IBOutlet weak var playAgainButton: UIButton!
    override func viewDidLoad() {
        
        generateLetters(self.letterIndex)
        
    }
    @IBOutlet weak var BackToMenuButton: UIButton!
    
    func increaseLetterSpan(first: Int, last: Int) {
        
        if(nextLetterSpan) {
            letterIndex[first] += 8
            letterIndex[last] += 8
        }
        nextLetterSpan = false
    }
  
    func generateLettersWhenTimerRings(timer : NSTimer) {
        generateLetters(timer.userInfo as! [Int])
    }
    
    func reactToLetterClick(b : UIButton) {
        if b.tag == 1 || b.tag == self.letterNumber {
            currentLetter = b.tag
            generateLetterSound()
            self.letterNumber += 1
            correctAnswers += 1
            if correctAnswers == 29 {
               
            }

            UIView.animateWithDuration(0.4, delay: 0, options: .CurveLinear, animations: {
                b.center.y = self.upperY
                b.center.x = self.upperX
                self.playLetterSoundTimer()
                }, completion: nil)
                upperX += 84
            if self.letterNumber == 5 || self.letterNumber == 13 || self.letterNumber == 21 {
                    upperX = 85
                    upperY = 200
                }
            if self.letterNumber == 29 {
                
                upperX = 210
                upperY = 200
            }
            if self.letterNumber == 9 || self.letterNumber == 17 || self.letterNumber == 25 {
                    self.upperX = 85
                    self.upperY = 120
                    nextLetterSpan = true
                    increaseLetterSpan(0, last: 7)
                    
                    removeLetterSpan()
                    newLetterSpan()
                    }
            }
    }
    
    func removeLetters () {
        for view in self.letterImage {
            view.removeFromSuperview()
        }
    }
    
    func randomLetter(inout letters: [Int]) -> Int {
        
        var randomIndex: UInt32
        randomIndex = (arc4random_uniform(UInt32(letters.count)))
        let currentNumber = (letters[Int(randomIndex)])
        letters.removeAtIndex(Int(randomIndex))
        return currentNumber
    }
    
    func generateLetters(letterspan: [Int]) {
        var col = 0
        for tagNr in letterspan.first!...letterspan.last! {
            let button = UIButton(frame: CGRectMake(self.positionX[col],self.positionY[col],100,100))
            button.tag = tagNr
            let name = String(tagNr)
            button.setImage(UIImage(named: name), forState: UIControlState.Normal)
            button.addTarget(self, action: "reactToLetterClick:", forControlEvents: UIControlEvents.TouchUpInside)
            self.view.addSubview(button)
            letterImage.append(button)
            col++
        }
    }
    
    func removeLetterSpan() {
        removeLetterTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "removeLetters", userInfo: nil, repeats: false)
    }
    
    func newLetterSpan() {
        newLetterTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "generateLettersWhenTimerRings:", userInfo: self.letterIndex, repeats: false)
    }
    
    func playLetterSoundTimer() {
        soundTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "playSound", userInfo: nil, repeats: false)
    }
    
    func playSuccesSoundTimer() {
        SuccesTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "generateSuccesSound", userInfo: nil, repeats: false)
    }
    
    func playSound() {
        if letterSound.playing {
            letterSound.stop()
        } else {
            letterSound.play()
        }
    }
    
    func generateLetterSound() {
        if currentLetter == 1 {
            let path = NSBundle.mainBundle().pathForResource("A", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 2 {
            let path = NSBundle.mainBundle().pathForResource("B", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 3 {
            let path = NSBundle.mainBundle().pathForResource("C", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 4 {
            let path = NSBundle.mainBundle().pathForResource("D", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 5 {
            let path = NSBundle.mainBundle().pathForResource("E", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 6 {
            let path = NSBundle.mainBundle().pathForResource("F", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 7 {
            let path = NSBundle.mainBundle().pathForResource("G", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 8 {
            let path = NSBundle.mainBundle().pathForResource("H", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 9 {
            let path = NSBundle.mainBundle().pathForResource("I", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 10 {
            let path = NSBundle.mainBundle().pathForResource("J", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 11 {
            let path = NSBundle.mainBundle().pathForResource("K", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 12 {
            let path = NSBundle.mainBundle().pathForResource("L", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 13 {
            let path = NSBundle.mainBundle().pathForResource("M", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 14 {
            let path = NSBundle.mainBundle().pathForResource("N", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 15 {
            let path = NSBundle.mainBundle().pathForResource("O", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 16 {
            let path = NSBundle.mainBundle().pathForResource("P", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 17 {
            let path = NSBundle.mainBundle().pathForResource("Q", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 18 {
            let path = NSBundle.mainBundle().pathForResource("R", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 19 {
            let path = NSBundle.mainBundle().pathForResource("S", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 20 {
            let path = NSBundle.mainBundle().pathForResource("T", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 21 {
            let path = NSBundle.mainBundle().pathForResource("U", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 22 {
            let path = NSBundle.mainBundle().pathForResource("V", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 23 {
            let path = NSBundle.mainBundle().pathForResource("W", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 24 {
            let path = NSBundle.mainBundle().pathForResource("X", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 25 {
            let path = NSBundle.mainBundle().pathForResource("Y", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 26 {
            let path = NSBundle.mainBundle().pathForResource("Z", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 27 {
            let path = NSBundle.mainBundle().pathForResource("Å", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 28 {
            let path = NSBundle.mainBundle().pathForResource("Ä", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
        } else if currentLetter == 29 {
            let path = NSBundle.mainBundle().pathForResource("Ö", ofType: "wav")
            soundUrl = NSURL(fileURLWithPath: path!)
            playSuccesSoundTimer()
            playAgainButton.hidden = false
            BackToMenuButton.hidden = false
        }
        
        do {
            try letterSound = AVAudioPlayer(contentsOfURL: soundUrl)
            letterSound.prepareToPlay()
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    func generateSuccesSound() {
        let path = NSBundle.mainBundle().pathForResource("braforsokigen", ofType: "wav")
        soundUrl = NSURL(fileURLWithPath: path!)
        
        do {
            try letterSound = AVAudioPlayer(contentsOfURL: soundUrl)
            letterSound.prepareToPlay()
        } catch let err as NSError {
            print(err.debugDescription)
        }
        playSound()
        removeLetters()
    }
    
    func resetGame() {
        letterIndex = [Int](1...8)
        letterNumber = 1
        correctAnswers = 0
        currentLetter = 0
        upperX = 85
        upperY = 120
        generateLetters(letterIndex)
        playAgainButton.hidden = true
        BackToMenuButton.hidden = true
    }
    
    @IBAction func playAgain(sender: AnyObject) {
       resetGame()
        
    }
}


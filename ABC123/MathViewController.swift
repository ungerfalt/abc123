//
//  MathViewController.swift
//  ABC123
//
//  Created by Daniel Ungerfält on 15/04/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//

import UIKit

class MathViewController: UIViewController {
    @IBOutlet weak var mathSpeechLabel: UILabel!
    
    var math = MathClass()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        mathSpeechLabel.text = math.addition[2]

    }
    
    @IBAction func buttonPressed(btn : UIButton!) {
        if btn.tag == math.answers[2] {
            print("BRA!")
        }
    }
    
    

  }

//
//  MathClass.swift
//  ABC123
//
//  Created by Daniel Ungerfält on 15/04/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//

import Foundation

class MathClass {
    
    var number: Int
    var num1 = Int(arc4random_uniform(UInt32(9)))
    var num2 = Int(arc4random_uniform(UInt32(9)))
    
  
    
    private var _addition : [String] = []
    var addition : [String] {
        return self._addition
    }
    private var _answers : [Int] = []
    var answers : [Int] {
        return self._answers
    }

    
    
    
    init() {
        
        
        self._addition = ["1 + 1 = ?","2 + 2 = ?","3 + 3 = ?"]
        
        self._answers = [2,4,6]
        
        self.number = 0
    }
    
    func addition(guess: Int, answer: Int) -> Bool {
        if guess == answer {
            return true
        }
        return false
    }
}

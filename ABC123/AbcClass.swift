//
//  AbcClass.swift
//  ABC123
//
//  Created by Daniel Ungerfält on 01/05/16.
//  Copyright © 2016 Daniel Ungerfält. All rights reserved.
//

import Foundation
import UIKit
import GameplayKit

class Abc {

var bgImage: UIImageView?
var letterIndex = [Int](1...8)
var image: UIImageView?
var letterNumber = 1
var upperX: CGFloat = 85
var upperY: CGFloat = 120
var isCorrect = false
var positionX : [CGFloat] = [208,114,20,302,302,20,208,114]
var positionY : [CGFloat] = [450,550,450,550,450,550,550,450]
var letterImage: [UIButton] = []
var tabelle : [UIButton] = []
var newLetterTimer = NSTimer()
var removeLetterTimer = NSTimer()
var math = MathClass()
var nextLetterSpan = false
    
    func increaseLetterSpan(first: Int, last: Int) {
        
        if(nextLetterSpan) {
            letterIndex[first] += 8
            letterIndex[last] += 8
        }
        nextLetterSpan = false
    }
    
    func removeLetters () {
        for view in self.letterImage {
            view.removeFromSuperview()
        }
    }
    
    func randomLetter(inout letters: [Int]) -> Int {
        
        var randomIndex: UInt32
        randomIndex = (arc4random_uniform(UInt32(letters.count)))
        let currentNumber = (letters[Int(randomIndex)])
        letters.removeAtIndex(Int(randomIndex))
        return currentNumber
        
    }
        
    func reactToLetterClick(b : UIButton) {
        
        if b.tag == 1 || b.tag == self.letterNumber {
            self.letterNumber += 1
            UIView.animateWithDuration(0.4, delay: 0, options: .CurveLinear, animations: {
                b.center.y = self.upperY
                b.center.x = self.upperX
                }, completion: nil)
            upperX += 84
            if self.letterNumber == 5 || self.letterNumber == 13 || self.letterNumber == 21 || self.letterNumber == 29{
                upperX = 85
                upperY = 200
                
            }
            if self.letterNumber == 9 || self.letterNumber == 17 || self.letterNumber == 25 {
                self.upperX = 85
                self.upperY = 120
                nextLetterSpan = true
                increaseLetterSpan(0, last: 7)
                
                removeLetterTimer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "removeLetters", userInfo: nil, repeats: false)
                
                newLetterTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: "generateLettersWhenTimerRings:", userInfo: self.letterIndex, repeats: false)
                
                
                
                //                    timer = NSTimer.scheduledTimerWithTimeInterval(3, target: self, selector: "removeLetters", userInfo:nil, repeats: false)
                
                //removeLetters()
            }
        }
        
    }



    
    

}



